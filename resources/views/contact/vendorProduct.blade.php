@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Vendor Product List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>Vendor</th>
                <th>Product ID</th>
                <th>Product Name</th>
            </thead>
            <tbody>
                @foreach($vendorProducts ?? '' as $vendorProduct)
                    <tr>
                        <td>{{ $vendorProduct->vendor ? $vendorProduct->vendor->name : "-"}}</td>
                        <td>{{ $vendorProduct->product->id }}</td>
                        <td>{{ $vendorProduct->product->product_name }}</td>
                    </tr>
                @endforeach
        </table>
        {{ $vendorProducts->links() }}
    </div>


@endsection
