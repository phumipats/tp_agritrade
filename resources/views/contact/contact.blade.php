@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Contact List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>Vendor</th>
                <th>Tax ID</th>
                <th>Billing Address</th>
                <th>Contact</th>
                <th>Branch</th>
                <th>Stock</th>
                <th>Expense</th>
                <th>Customer</th>
            </thead>
            <tbody>
                @foreach($contacts ?? '' as $contact)
                    <tr>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->tax_id }}</td>
                        <td>{{ $contact->billing_address }}</td>
                        <td>{{ $contact->contact }}</td>
                        <td>{{ $contact->branch }}</td>
                        <td>{{ $contact->stock }}</td>
                        <td>{{ $contact->expense }}</td>
                        <td>{{ $contact->customer }}</td>
                    </tr>
                @endforeach
        </table>
        {{ $contacts->links() }}
    </div>


@endsection
