@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Warehouse List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>Warehouse ID</th>
                <th>Warehouse Name</th>
            </thead>
            <tbody>
                @foreach($warehouses ?? '' as $warehouse)
                    <tr>
                        <td>{{ $warehouse->id }}</td>
                        <td>{{ $warehouse->name }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $warehouses->links() }}
    </div>


@endsection
