@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Pig House List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>Branch</th>
                <th>House Name</th>
                <th>IPC</th>
                <th>PLC IP</th>
            </thead>
            <tbody>
                @foreach($pighouses ?? '' as $pighouse)
                    <tr>
                        <td>{{ $pighouse->branch_id }}</td>
                        <td>{{ $pighouse->name }}</td>
                        <td>{{ $pighouse->ipc }}</td>
                        <td>{{ $pighouse->plc_ip }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $pighouses->links() }}
    </div>


@endsection
