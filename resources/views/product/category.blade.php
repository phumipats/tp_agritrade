@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Product Category</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Group Name</th>
                <th>Unit</th>
            </thead>
            <tbody>
                @foreach($p_Categories ?? '' as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->product_group->group_name }}</td>
                        <td>{{ $category->unit->name}}</td>

                    </tr>
                @endforeach
        </table>
        {{ $p_Categories->links() }}
    </div>


@endsection
