@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Product Group</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Group Name</th>
            </thead>
            <tbody>
                @foreach($p_Groups ?? '' as $group)
                    <tr>
                        <td>{{ $group->id }}</td>
                        <td>{{ $group->group_name }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $p_Groups->links() }}
    </div>


@endsection
