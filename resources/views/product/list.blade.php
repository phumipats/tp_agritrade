@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Product List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>Product Name</th>
                <th>Product Category</th>
                <th>Receive Unit</th>
                <th>Convert Unit</th>
                <th>Ratio</th>
                <th>Inventory Unit</th>
                <th>ID</th>
                <th>Inventory</th>
            </thead>
            <tbody>
                @foreach($products ?? '' as $product)
                    <tr>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->product_category->name }}</td>
                        <td>{{ $product->runit->name }}</td>
                        <td>{{ $product->convert_unit }}</td>
                        <td>{{ $product->ratio }}</td>
                        <td>{{ $product->inventory_unit }}</td>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->inventory }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $products->links() }}
    </div>


@endsection
