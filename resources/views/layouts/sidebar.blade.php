<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light nav-justified">
  <div class="container-fluid">
    <a class="navbar-brand" href="/home">ArgriTrade{{ Request::get('a') }}</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  
        <!-- Invoice -->
        <li class="nav-item">
            <div class="btn-group nav-item">
                <a class="nav-link" href="/buy/invoice">Invoices</a>
                <a type="button" class="nav-link dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-bs-toggle="dropdown" aria-expanded="false" data-bs-reference="parent">
                    <span class="visually-hidden">Toggle Dropdown</span>
                </a>
                <ul class="dropdown-menu " aria-labelledby="dropdownMenuReference">
                    <li><a class="dropdown-item" href="/buy/receive">Receive</a></li>
                    <li><a class="dropdown-item" href="/buy/order-list">Order</a></li>
                    <li><a class="dropdown-item" href="/buy/payment">Payment</a></li>
                </ul>
            </div>
        </li>

        <!-- Contact -->
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/contact/list">Contacts</a>
        </li>

        <!-- Warehouse -->
        <li class="nav-item">
            <div class="btn-group nav-item">
                <a class="nav-link" href="/warehouse">Warehouse</a>
                <a type="button" class="nav-link dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-bs-toggle="dropdown" aria-expanded="false" data-bs-reference="parent">
                    <span class="visually-hidden">Toggle Dropdown</span>
                </a>
                <ul class="dropdown-menu " aria-labelledby="dropdownMenuReference">
                    <li><a class="dropdown-item" href="/pighouse">Pighouse</a></li>
                </ul>
            </div>
        </li>
        <!-- Product -->
        <li class="nav-item">
            <div class="btn-group nav-item">
                <a class="nav-link" href="/product">Product</a>
                <a type="button" class="nav-link dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference" data-bs-toggle="dropdown" aria-expanded="false" data-bs-reference="parent">
                    <span class="visually-hidden">Toggle Dropdown</span>
                </a>
                <ul class="dropdown-menu " aria-labelledby="dropdownMenuReference">
                    <li><a class="dropdown-item" href="/product-category">Category</a></li>
                    <li><a class="dropdown-item" href="/product-group">Group</a></li>
                    <li><a class="dropdown-item" href="/contact/vendor-product">Vendors</a></li>
                </ul>
            </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
