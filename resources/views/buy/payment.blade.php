@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Payment</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Invoice ID</th>
                <th>Name</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Grand Total</th>
                <th>Comment</th>
            </thead>
            <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td>{{ $payment->id }}</td>
                        <td>{{ $payment->invoice_id }}</td>
                        <td>{{ $payment->vendor_id }}</td>
                        <td>{{ $payment->vendor_name }}</td>
                        <td>{{ $payment->due_date }}</td>
                        <td>{{ $payment->status_id }}</td>
                        <td>{{ $payment->grand_total}}</td>
                        <td>{{ $payment->note}}</td>

                    </tr>
                @endforeach
        </table>
        {{ $payments->links() }}
    </div>


@endsection
