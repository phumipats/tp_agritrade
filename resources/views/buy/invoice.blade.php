@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Invoice List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>#Invoice</th>
                <th>Name</th>
                <th>Ship Date</th>
                <th>Due Date</th>
                <th>Orders</th>
                <th>Status</th>
            </thead>
            <tbody>
                @foreach($invoices ?? '' as $invoice)
                    <tr>
                        <td>{{ $invoice->id }}</td>
                        <td>{{ $invoice->invoice_no }}</td>
                        <td>{{ $invoice->vendor->name }}</td>
                        <td>{{ $invoice->ship_date }}</td>
                        <td>{{ $invoice->due_date }}</td>
                        <td>{{ $invoice->number_of_record }}</td>
                        <td>{{ $invoice->status_id }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $invoices->links() }}
    </div>


@endsection
