@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Payment Detail</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Vendor</th>
                <th>Issue Date</th>
                <th>Type</th>
                <th>Bank</th>
                <th>#Account</th>
                <th>Payment Date</th>
                <th>Reference No.</th>
                <th>Status</th>
            </thead>
            <tbody>
                @foreach($payments ?? '' as $payment)
                    <tr>
                        <td>{{ $payment->id }}</td>
                        <td>{{ $payment->vendor->name }}</td>
                        <td>{{ $payment->issue_date }}</td>
                        <td>{{ $payment->payment_method->type }}</td>
                        <td>{{ $payment->bankaccount ? $payment->bankaccount->bank_prop->name : '-'}}</td>
                        <td>{{ $payment->bankaccount ? $payment->bankaccount->account_no : '-' }}</td>
                        <td>{{ $payment->payment_date }}</td>
                        <td>{{ $payment->reference_no }}</td>
                        <td>{{ $payment->status_id }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $payments->links() }}
    </div>


@endsection
