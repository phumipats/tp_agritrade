@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Order List</h2>

        <table class="table table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Invoice ID</th>
                <th>Name</th>
                <th>#Invoice</th>
                <th>Product ID</th>
                <th>Product name</th>
                <th>Amount</th>
                <th>Unit</th>
                <th>Warehouse</th>
            </thead>
            <tbody>
                @foreach($orders ?? '' as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->invoice_id }}</td>
                        <td>{{ $order->vendor_id }}</td>
                        <td>{{ $order->invoice_no }}</td>
                        <td>{{ $order->product->id }}</td>
                        <td>{{ $order->product->product_name }}</td>
                        <td>{{ $order->amount }}</td>
                        <td>{{ $order->unit_id }}</td>
                        <td>{{ $order->warehouse_id }}</td>

                    </tr>
                @endforeach
        </table>
        {{ $orders->links() }}
    </div>


@endsection
