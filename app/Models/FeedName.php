<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedName extends Model
{
    use HasFactory;

    protected $connection = 'mysqlfeed';
    protected $table = 'name';
}
