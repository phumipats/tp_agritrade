<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;

    protected $table = 'warehouse';

    public function pigHouses()
    {
        return $this->hasMany(PigHouse::class, 'warehouse_id');
    }
}
