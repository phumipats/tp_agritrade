<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedMix extends Model
{
    use HasFactory;

    protected $connection = 'mysqlfeed';
    protected $table = 'mix';
}
