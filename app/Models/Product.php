<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class,'product_category_id','id');
    }

    public function runit() {
        return $this->belongsTo(Unit::class,'receive_unit_id','id');
    }

    public function cunit() {
        return $this->belongsTo(Unit::class,'convert_unit','id');
    }
}
