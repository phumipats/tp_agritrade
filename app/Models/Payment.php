<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $table = 'payment';

    public function vendor(){
        return $this->belongsTo(Contact::class,'vendor_id','id');
    }

    public function payment_method(){
        return $this->belongsTo(PaymentMethod::class,'method','id');
    }

    public function bankaccount(){
        return $this->belongsTo(BankAccount::class,'bank_account','id');
    }
}
