<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public static $wrap = 'product';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'product_category' => $this->whenLoaded('product_category'),
            'receive_unit' => $this->receive_unit,
            'convert_unit' => $this->convert_unit,
            'ratio' => $this->ratio,
            'inventory_unit' => $this->inventory_unit,
            'inventory' => $this->inventory,
        ];
    }
}
