<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\Contact;
use App\Models\Cooking;
use App\Models\Feed;
use App\Models\FeedMix;
use App\Models\FeedName;
use App\Models\FeedRecipe;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\InvoiceStatus;
use App\Models\Order;
use App\Models\Payment;
use App\Models\PaymentMethod;
use App\Models\PigHouse;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\Receipt;
use App\Models\Unit;
use App\Models\UnitConvert;
use App\Models\VendorProduct;
use App\Models\Warehouse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        // $test = FeedRecipe::all();
        // return $test;
        return view('home');
    }

    // main tab

    // buy tab
    public function invoice(){
        $invoices = Invoice::latest('id')->paginate(10);
        return view('buy.invoice', compact('invoices'));
    }

    public function receive(){
        // $receipts = Receipt::latest('id')->paginate(10);
        // return view('buy.receive', compact('receipts'));
    }

    public function expense(){
        // $expenses = Expense::latest('id')->paginate(10);
        // return view('buy.expense', compact('expenses'));
    }

    public function orderList(){
        $orders = Order::latest('id')->paginate(10);
        return view('buy.orderlist', compact('orders'));
    }

    public function payment(){
        $payments = Payment::latest('id')->paginate(10);
        return view('buy.payment', compact('payments'));
    }

    public function paymentDetail(){
        $payments = Payment::latest('id')->paginate(10);
        return view('buy.paymentdetail', compact('payments'));
    }



    // inventory tab

    // contact tab
    public function contact()
    {
        $contacts = Contact::paginate(10);
        // return $contacts;
        return view('contact.contact', [
            'contacts' => $contacts,
        ]);

    }

    public function vendorProduct(){
        $vendorProducts = VendorProduct::paginate(10);
        return view('contact.vendorproduct', compact('vendorProducts'));
    }

    // warehouse tab
    public function warehouse(){
        $warehouses = Warehouse::paginate(10);
        return view('warehouse.warehouse', [
            'warehouses' => $warehouses,
        ]);
    }

    public function pighouse(){
        $pighouses = PigHouse::paginate(10);
        return view('warehouse.pighouse', [
            'pighouses' => $pighouses,
        ]);
    }

    // product tab
    public function productList(){
        $products = Product::paginate(10);
        // return $products;
        return view('product.list',[
            'products' => $products
        ]);
    }

    public function productCategory(){
        $productCate = ProductCategory::paginate(10);
        return view('product.category',[
            'p_Categories' => $productCate
        ]);
    }

    public function productGroup(){
        $productGrps = ProductGroup::paginate(10);
        return view('product.group',[
            'p_Groups' => $productGrps
        ]);
    }

    // stock tab
    public function stock(){

    }

    // feed tab



}
