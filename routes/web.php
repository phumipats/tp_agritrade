<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [HomeController::class, 'index']);



Route::get('/buy/invoice', [HomeController::class, 'invoice']);
Route::get('/buy/receive', [HomeController::class, 'receive']);
Route::get('/buy/order-list', [HomeController::class, 'orderList']);
Route::get('/buy/payment', [HomeController::class, 'payment']);
Route::get('/buy/payment-detail', [HomeController::class, 'paymentDetail']);

Route::get('/contact/list', [HomeController::class, 'contact']);
Route::get('/contact/vendor-product', [HomeController::class, 'vendorProduct']);
Route::get('/warehouse', [HomeController::class, 'warehouse']);
Route::get('/pighouse', [HomeController::class, 'pighouse']);
Route::get('/product', [HomeController::class, 'productList']);
Route::get('/product-category', [HomeController::class, 'productCategory']);
Route::get('/product-group', [HomeController::class, 'productGroup']);



// Route::view('/contact', 'contact');
